Command line instructions
You can also upload existing files from your computer using the instructions below.


Git global setup
git config --global user.name "Alesson DJesus"
git config --global user.email "lesson9006@gmail.com"

Create a new repository
git clone git@gitlab.com:lesson9006/site-agencia-polifocus.git
cd site-agencia-polifocus
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push an existing folder
cd existing_folder
git init
git remote add origin git@gitlab.com:lesson9006/site-agencia-polifocus.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:lesson9006/site-agencia-polifocus.git
git push -u origin --all
git push -u origin --tags